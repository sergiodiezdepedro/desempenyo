const gulp = require('gulp');
const scss = require('gulp-dart-scss');
const autoprefixer = require ('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const plumber = require('gulp-plumber');
const browserSync = require('browser-sync').create();

function estilos() { 
  return gulp.src('./scss/**/*.scss')
  .pipe(scss({}))
  .pipe(plumber())
  .pipe(autoprefixer())
  .pipe(cssnano({
    core:true
  }))
  .pipe(gulp.dest('./css'))
  .pipe(browserSync.stream());

 }

 function watch() {
   browserSync.init({
     server: {
       baseDir: './'
     } 
   });

   gulp.watch('./scss/**/*.scss', estilos);
   gulp.watch('./*.html').on('change', browserSync.reload);
 }

 exports.estilos = estilos;
 exports.watch = watch;